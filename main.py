import sys

def main():
    number = int(sys.argv[1])
    for i in range(number):
        print(str(i) + ' ' + str(i ** 2))

if __name__ == '__main__':
    main()