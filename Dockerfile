FROM python:3.6-jessie

WORKDIR /opt
ADD / /opt
ENV url=$url

ENTRYPOINT python -u /opt/url.py $url