import urllib.request
import sys

def main():
    urls = sys.argv[1]
    for url in urls.split(","):
        print(url)
        try:
            r = urllib.request.urlopen(url).read()
            print(r)
        except ValueError:
            print("The connection didn't succeed")
            sys.exit()


if __name__ == "__main__":
    main()